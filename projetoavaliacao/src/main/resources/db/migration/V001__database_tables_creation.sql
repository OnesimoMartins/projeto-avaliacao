CREATE TABLE reacao_adversa(
id serial primary key,
descricao varchar(200)
);

CREATE TABLE fabricante(
id SERIAL PRIMARY KEY,
nome VARCHAR(100) NOT NULL
);

CREATE TABLE medicamento(
numero_registro VARCHAR(17) PRIMARY KEY,
nome VARCHAR(50) NOT NULL,
data_validade DATE NOT NULL,
telefone_SAC VARCHAR(13) NOT NULL,
preco NUMERIC(19,2) NOT NULL,
quantidade_comprimidos SMALLINT NOT NULL,
fabricante INTEGER references fabricante(id)
);

CREATE TABLE reacao_adversa_medicamento(
reacao_adversa_id INTEGER NOT NULL references reacao_adversa(id),
medicamento_numero_registro VARCHAR(17) NOT NULL references medicamento(numero_registro)
);
