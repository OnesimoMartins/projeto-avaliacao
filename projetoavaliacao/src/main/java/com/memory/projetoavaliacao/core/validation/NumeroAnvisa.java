package com.memory.projetoavaliacao.core.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Pattern;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Pattern(regexp = "^[0-9]\\.[0-9]{4}\\.[0-9]{4}\\.[0-9]{3}\\-[0-9]$")
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD,ElementType.FIELD})
@Constraint(validatedBy = {})
public @interface NumeroAnvisa {

    String message() default "Número de registro Anvisa inválido";
    Class<?>[] groups() default { };
    Class<? extends Payload>[] payload() default { };
}
