package com.memory.projetoavaliacao.core.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@EnableSwagger2
@Configuration
public class SwaggerConfig {

    @Bean
    @Primary
    public SwaggerResourcesProvider swaggerResourcesProvider() {
        return ()-> Collections.singletonList(swaggerResource());
    }

    private SwaggerResource swaggerResource() {
        SwaggerResource swaggerResource = new SwaggerResource();
        swaggerResource.setName("Projeto Avalicao");
        swaggerResource.setSwaggerVersion("2.0");
        swaggerResource.setLocation("/swagger/swagger.yaml");

        return swaggerResource;
    }
}
