package com.memory.projetoavaliacao.api.controller;

import com.memory.projetoavaliacao.api.dto.input.ReacaoAdversaInput;
import com.memory.projetoavaliacao.api.dto.mapper.ReacaoAdversaMapper;
import com.memory.projetoavaliacao.api.dto.output.ReacaoAdversaOutput;
import com.memory.projetoavaliacao.domain.entity.ReacaoAdversa;
import com.memory.projetoavaliacao.domain.service.ReacaoAdversaService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("reacoes-adversas")
@RequiredArgsConstructor
public class ReacaoAdversaController {

    private final ReacaoAdversaService reacaoAdversaService;
    private final ReacaoAdversaMapper reacaoAdversaMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ReacaoAdversaOutput incluir(@RequestBody @Valid ReacaoAdversaInput reacaoAdversaInput) {

        ReacaoAdversa reacaoAdversa = reacaoAdversaMapper.toReacaoAdversa(reacaoAdversaInput);
        reacaoAdversa=reacaoAdversaService.salvar(reacaoAdversa);

        return reacaoAdversaMapper.toReacaoAdversaOutput(reacaoAdversa);
    }

    @PutMapping("{reacaoAdversaId}")
    public ReacaoAdversaOutput atualizar(@RequestBody @Valid ReacaoAdversaInput reacaoAdversaInput,
                                         @PathVariable Long reacaoAdversaId) {
        ReacaoAdversa reacaoAdversaAtual= reacaoAdversaService.buscarPorId(reacaoAdversaId);
        reacaoAdversaAtual=reacaoAdversaMapper.copiarParaReacaoAdversa(reacaoAdversaInput,reacaoAdversaAtual);
        reacaoAdversaAtual=reacaoAdversaService.salvar(reacaoAdversaAtual);

        return reacaoAdversaMapper.toReacaoAdversaOutput(reacaoAdversaAtual);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("{reacaoAdversaId}")
    public void excluir(@PathVariable Long reacaoAdversaId) {
        reacaoAdversaService.eliminar(reacaoAdversaId);
    }




}
