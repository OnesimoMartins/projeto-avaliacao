package com.memory.projetoavaliacao.api.dto.output;

import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Getter
@Builder
public class MedicamentoOutput {

    private String numeroRegistro;
    private String nome;
    private Date dataValidade;
    private Integer quantidadeComprimidos;
    private BigDecimal preco;
    private  String telefoneSac;
    private FabricanteOutput fabricante;
    private List<ReacaoAdversaOutput> reacoesAdversas;
}
