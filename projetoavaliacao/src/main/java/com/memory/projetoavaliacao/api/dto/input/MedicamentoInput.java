package com.memory.projetoavaliacao.api.dto.input;

import com.memory.projetoavaliacao.core.validation.NumeroAnvisa;
import com.memory.projetoavaliacao.core.validation.NumeroTelefone;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MedicamentoInput {

    @NumeroAnvisa
    @NotBlank
    private String numeroRegistro;

    @NotBlank(groups = Atualizar.class)
    private String nome;

    @NotNull(groups = Atualizar.class)
    private BigDecimal preco;

    @NotBlank(groups = Atualizar.class)
    @NumeroTelefone(groups = Atualizar.class)
    private String telefoneSac;

    @NotNull(groups = Atualizar.class)
    private Date dataValidade;

    @Positive(groups = Atualizar.class)
    @NotNull(groups = Atualizar.class)
    private Integer quantidadeComprimidos;

    @NotNull(groups = Atualizar.class)
    private Long fabricanteId;

    private Set<Long> reacoesAdversasIds=new HashSet<>();

    public  interface Atualizar{}
}
