package com.memory.projetoavaliacao.api.dto.output;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ReacaoAdversaOutput {
    private Long id;
    private String descricao;
}
