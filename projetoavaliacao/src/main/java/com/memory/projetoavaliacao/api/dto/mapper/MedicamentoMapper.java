package com.memory.projetoavaliacao.api.dto.mapper;

import com.memory.projetoavaliacao.api.dto.input.MedicamentoInput;
import com.memory.projetoavaliacao.api.dto.output.MedicamentoOutput;
import com.memory.projetoavaliacao.domain.entity.Fabricante;
import com.memory.projetoavaliacao.domain.entity.Medicamento;
import com.memory.projetoavaliacao.domain.entity.ReacaoAdversa;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class MedicamentoMapper {

    private final ReacaoAdversaMapper reacaoAdversaMapper;
    private final FabricanteMapper fabricanteMapper;

    public MedicamentoOutput toMedicamentoOutput(Medicamento medicamento){
       return MedicamentoOutput.builder()
               .nome(medicamento.getNome())
               .preco(medicamento.getPreco())
               .fabricante(fabricanteMapper.toFabricanteOutput(medicamento.getFabricante()))
               .quantidadeComprimidos(medicamento.getQuantidadeComprimidos())
               .dataValidade(medicamento.getDataValidade())

               .numeroRegistro(medicamento.getNumeroRegistro())
               .telefoneSac(medicamento.getTelefoneSac())
               .reacoesAdversas(medicamento.getReacoesAdversas().stream()
                       .map(reacaoAdversaMapper::toReacaoAdversaOutput).collect(Collectors.toList()))
               .build();

    }

    public Medicamento toMedicamentoDomainObject(MedicamentoInput medicamentoInput){
        return Medicamento.builder()
                .numeroRegistro(medicamentoInput.getNumeroRegistro())
                .dataValidade(medicamentoInput.getDataValidade())
                .fabricante(new Fabricante(medicamentoInput.getFabricanteId(), null))
                .nome(medicamentoInput.getNome())
                .quantidadeComprimidos(medicamentoInput.getQuantidadeComprimidos())
                .preco(medicamentoInput.getPreco())
                .telefoneSac(medicamentoInput.getTelefoneSac())
                .reacoesAdversas(medicamentoInput
                        .getReacoesAdversasIds()
                        .stream().map(i->new ReacaoAdversa(i,null)).collect(Collectors.toSet()))
                .build();
    }

    public Medicamento copiarParaMedicamento(MedicamentoInput medicamentoInput, Medicamento medicamentoActual){
        Medicamento medicamento=toMedicamentoDomainObject(medicamentoInput);

        medicamentoActual.setDataValidade(medicamento.getDataValidade());
        medicamentoActual.setNome(medicamento.getNome());
        medicamentoActual.setFabricante(medicamento.getFabricante());
        medicamentoActual.setReacoesAdversas(medicamento.getReacoesAdversas());
        medicamentoActual.setPreco(medicamento.getPreco());
        medicamentoActual.setTelefoneSac(medicamento.getTelefoneSac());
        medicamentoActual.setQuantidadeComprimidos(medicamento.getQuantidadeComprimidos());

        return medicamentoActual;
    }

    public Page<MedicamentoOutput> toMedicamentoOutputPage(Page<Medicamento> medicamentos){

        List<MedicamentoOutput> medicamentoOutputList= medicamentos.stream()
                .map(this::toMedicamentoOutput).collect(Collectors.toList());

        return new PageImpl<>(medicamentoOutputList);

    }
}
