package com.memory.projetoavaliacao.api.controller;

import com.memory.projetoavaliacao.api.dto.output.ErrorResponse;
import com.memory.projetoavaliacao.domain.exception.MedicamentoJaExistenteException;
import com.memory.projetoavaliacao.domain.exception.ReacaoAdversaEmUsoEception;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;

@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<?> handleEntityNotFoundException(EntityNotFoundException ex, WebRequest req) {
        ErrorResponse erroResponse = ErrorResponse.builder()
                .status(404)
                .descricao(ex.getMessage())
                .build();

        return handleExceptionInternal(ex, erroResponse, new HttpHeaders(), HttpStatus.NOT_FOUND, req);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(MedicamentoJaExistenteException.class)
    public ResponseEntity<?> handleMedicamentoJaExistenteException(MedicamentoJaExistenteException ex, WebRequest req) {
        ErrorResponse erroResponse = ErrorResponse.builder()
                .status(400)
                .descricao(ex.getMessage())
                .build();

        return handleExceptionInternal(ex, erroResponse, new HttpHeaders(), HttpStatus.NOT_FOUND, req);
    }
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(ReacaoAdversaEmUsoEception.class)
    public ResponseEntity<?> handleReacaoAdversaEmUsoEception(ReacaoAdversaEmUsoEception ex, WebRequest req) {
        ErrorResponse erroResponse = ErrorResponse.builder()
                .status(404)
                .descricao(ex.getMessage())
                .build();

        return handleExceptionInternal(ex, erroResponse, new HttpHeaders(), HttpStatus.NOT_FOUND, req);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ErrorResponse errorResponse = ErrorResponse.builder()
                .status(400)
                .descricao("Erro no corpo da requisição, verifique os campos.")
                .build();

        return handleExceptionInternal(ex, errorResponse, headers, status, request);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleExceptionUncaught(Exception ex, WebRequest webRequest) {
        ErrorResponse errorResponse = ErrorResponse.builder()
                .status(500)
                .descricao("Erro interno do servidor.")
                .build();

        return handleExceptionInternal(ex, errorResponse, new HttpHeaders(), HttpStatus.valueOf(500), webRequest);
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body,
                                                             HttpHeaders headers, HttpStatus status, WebRequest request) {

        if (body == null) {
            body = ErrorResponse.builder()
                    .timestamp(LocalDateTime.now())
                    .status(status.value())
                    .descricao(ex.getMessage())
                    .build();
        } else if (body instanceof String) {
            body = ErrorResponse.builder()
                    .descricao((String) body)
                    .timestamp(LocalDateTime.now())
                    .status(status.value())
                    .build();
        } else if (body instanceof ErrorResponse) {
            ((ErrorResponse) body).setTimestamp(LocalDateTime.now());
        }

        return super.handleExceptionInternal(ex, body, headers, status, request);
    }
}
