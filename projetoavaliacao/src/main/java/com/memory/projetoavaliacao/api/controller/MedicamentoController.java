package com.memory.projetoavaliacao.api.controller;

import com.memory.projetoavaliacao.api.dto.input.MedicamentoInput;
import com.memory.projetoavaliacao.api.dto.mapper.MedicamentoMapper;
import com.memory.projetoavaliacao.api.dto.output.MedicamentoOutput;
import com.memory.projetoavaliacao.domain.entity.Medicamento;
import com.memory.projetoavaliacao.domain.filter.MedicamentoFiltro;
import com.memory.projetoavaliacao.domain.service.MedicamentoService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("medicamentos")
@RequiredArgsConstructor
public class MedicamentoController {

    public final MedicamentoService medicamentoService;
    public final MedicamentoMapper medicamentoMapper;

    @GetMapping
    public Page<MedicamentoOutput> pesquisar(MedicamentoFiltro filtro, @PageableDefault Pageable pageable) {
        Page<Medicamento> medicamentos = medicamentoService.filtrarMedicamentos(filtro, pageable);

        return medicamentoMapper.toMedicamentoOutputPage(medicamentos);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public MedicamentoOutput incluir(@RequestBody @Valid MedicamentoInput medicamentoInput) {
        Medicamento medicamento = medicamentoMapper.toMedicamentoDomainObject(medicamentoInput);
        medicamento = medicamentoService.criar(medicamento);

        return medicamentoMapper.toMedicamentoOutput(medicamento);
    }

    @PutMapping("{numeroMedicamento}")
    public MedicamentoOutput atualizar(@PathVariable String numeroMedicamento,
                                       @RequestBody @Validated(MedicamentoInput.Atualizar.class) MedicamentoInput medicamentoInput) {
        Medicamento medicamentoAtual = medicamentoService.buscarPorNumeroRegistro(numeroMedicamento);
        medicamentoAtual = medicamentoMapper.copiarParaMedicamento(medicamentoInput, medicamentoAtual);
        medicamentoAtual = medicamentoService.atualizar(medicamentoAtual);

        return medicamentoMapper.toMedicamentoOutput(medicamentoAtual);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("{numeroMedicamento}")
    public void eliminar(@PathVariable String numeroMedicamento) {
        medicamentoService.eliminar(numeroMedicamento);
    }


}
