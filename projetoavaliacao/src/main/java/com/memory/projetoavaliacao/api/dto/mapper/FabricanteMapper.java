package com.memory.projetoavaliacao.api.dto.mapper;

import com.memory.projetoavaliacao.api.dto.output.FabricanteOutput;
import com.memory.projetoavaliacao.domain.entity.Fabricante;
import org.springframework.stereotype.Component;

@Component
public class FabricanteMapper {

    public FabricanteOutput toFabricanteOutput(Fabricante Fabricante){
        return FabricanteOutput.builder()
                .id(Fabricante.getId())
                .nome(Fabricante.getNome())
                .build();
    }
}
