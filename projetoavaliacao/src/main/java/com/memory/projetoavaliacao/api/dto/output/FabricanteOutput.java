package com.memory.projetoavaliacao.api.dto.output;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class FabricanteOutput {
    private Long id;
    private String nome;
}
