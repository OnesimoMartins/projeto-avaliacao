package com.memory.projetoavaliacao.api.dto.mapper;

import com.memory.projetoavaliacao.api.dto.input.ReacaoAdversaInput;
import com.memory.projetoavaliacao.api.dto.output.ReacaoAdversaOutput;
import com.memory.projetoavaliacao.domain.entity.ReacaoAdversa;
import org.springframework.stereotype.Component;

@Component
public class ReacaoAdversaMapper {

    public ReacaoAdversa toReacaoAdversa(ReacaoAdversaInput input){
        ReacaoAdversa reacaoAdversa=new ReacaoAdversa();
        reacaoAdversa.setDescricao(input.getDescricao());

        return reacaoAdversa;
    }

    public ReacaoAdversa copiarParaReacaoAdversa(ReacaoAdversaInput reacaoAdversaInput,
                                                 ReacaoAdversa reacaoAdversa){
        reacaoAdversa.setDescricao(reacaoAdversaInput.getDescricao());

        return reacaoAdversa;
    }

    public ReacaoAdversaOutput toReacaoAdversaOutput(ReacaoAdversa reacaoAdversa){
        return ReacaoAdversaOutput.builder()
                .id(reacaoAdversa.getId())
                .descricao(reacaoAdversa.getDescricao())
                .build();
    }
}
