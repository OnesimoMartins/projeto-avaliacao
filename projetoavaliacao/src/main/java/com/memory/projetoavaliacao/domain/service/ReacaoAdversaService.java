package com.memory.projetoavaliacao.domain.service;

import com.memory.projetoavaliacao.domain.entity.ReacaoAdversa;

public interface ReacaoAdversaService {

    ReacaoAdversa salvar(ReacaoAdversa reacaoAdversa);

    ReacaoAdversa buscarPorId(Long id);

    void eliminar(Long id);


}
