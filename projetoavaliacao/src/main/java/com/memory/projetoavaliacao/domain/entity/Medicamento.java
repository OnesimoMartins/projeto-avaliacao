package com.memory.projetoavaliacao.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

@Getter
@Setter
@Builder
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Medicamento {

    @Id
    private String numeroRegistro;
    private String nome;
    private Date dataValidade;
    private Integer quantidadeComprimidos;
    private BigDecimal preco;
    private String telefoneSac;

    @ManyToOne
    private Fabricante fabricante;

    @ManyToMany
    @JoinTable(name = "reacao_adversa_medicamento",joinColumns = @JoinColumn(
            name = "medicamento_numero_registro"),
            inverseJoinColumns = @JoinColumn(name = "reacao_adversa_id"))
    private Set<ReacaoAdversa> reacoesAdversas;
}
