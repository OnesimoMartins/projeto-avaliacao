package com.memory.projetoavaliacao.domain.exception;

public class ReacaoAdversaEmUsoEception extends RuntimeException{
    public ReacaoAdversaEmUsoEception(String msg) {
        super(msg);
    }
}
