package com.memory.projetoavaliacao.domain.service;

import com.memory.projetoavaliacao.domain.entity.Medicamento;
import com.memory.projetoavaliacao.domain.filter.MedicamentoFiltro;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityNotFoundException;

public interface MedicamentoService {

    Medicamento buscarPorNumeroRegistro(String numeroRegistro) throws EntityNotFoundException;

    Medicamento criar(Medicamento medicamento);

    Medicamento atualizar(Medicamento medicamento);

    void eliminar(String numeroRegistro );

    Page<Medicamento> filtrarMedicamentos(MedicamentoFiltro medicamentoFilter, Pageable pageable);

}
