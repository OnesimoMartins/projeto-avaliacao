package com.memory.projetoavaliacao.domain.filter;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MedicamentoFiltro  {

    private String numeroRegistro;
    private String nome;

}
