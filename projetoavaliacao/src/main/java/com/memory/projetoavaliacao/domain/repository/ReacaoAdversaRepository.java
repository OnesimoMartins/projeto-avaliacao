package com.memory.projetoavaliacao.domain.repository;

import com.memory.projetoavaliacao.domain.entity.ReacaoAdversa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReacaoAdversaRepository extends JpaRepository<ReacaoAdversa,Long> {

}