package com.memory.projetoavaliacao.domain.repository;

import com.memory.projetoavaliacao.domain.entity.Fabricante;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FabricanteRepository extends JpaRepository<Fabricante,Long> {
}
