package com.memory.projetoavaliacao.domain.exception;


public class MedicamentoJaExistenteException extends RuntimeException {

    public MedicamentoJaExistenteException(String  msg){
        super(msg);
    }
}
