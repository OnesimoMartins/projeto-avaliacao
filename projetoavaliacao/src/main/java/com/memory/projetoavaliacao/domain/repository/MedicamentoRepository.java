package com.memory.projetoavaliacao.domain.repository;

import com.memory.projetoavaliacao.domain.entity.Medicamento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MedicamentoRepository extends JpaRepository<Medicamento, String>, JpaSpecificationExecutor<Medicamento> {
}
