package com.memory.projetoavaliacao.domain.service;

import com.memory.projetoavaliacao.domain.entity.Fabricante;

public interface FabricanteService {

    Fabricante buscarPorId(Long id);
}
