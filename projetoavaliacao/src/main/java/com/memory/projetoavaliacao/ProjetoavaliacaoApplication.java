package com.memory.projetoavaliacao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetoavaliacaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetoavaliacaoApplication.class, args);
	}

}
