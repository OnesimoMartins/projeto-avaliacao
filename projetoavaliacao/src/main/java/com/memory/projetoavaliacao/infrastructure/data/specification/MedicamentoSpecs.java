package com.memory.projetoavaliacao.infrastructure.data.specification;

import com.memory.projetoavaliacao.domain.entity.Medicamento;
import com.memory.projetoavaliacao.domain.filter.MedicamentoFiltro;
import org.springframework.data.jpa.domain.Specification;

public class MedicamentoSpecs {

    /*
    *  especificação para a consulta de medicamentos, procura medicamentos com nomes ou número de
    *  registro idênticos ao fornecido, dando prioridade ao número de registro
    * */
    public static Specification<Medicamento> comNomeOuNumeroRegistroSemelhante(MedicamentoFiltro filtro){
        return (root, query, criteriaBuilder) -> {

            if (filtro.getNumeroRegistro()!=null) {
                 return criteriaBuilder.like(root.get("numeroRegistro"),"%"+filtro.getNumeroRegistro()+"%");
            }
            else if (filtro.getNome()!=null){
                return criteriaBuilder.like(root.get("nome"),"%"+filtro.getNome()+"%");
            }

            return null;
        };
    }

}
