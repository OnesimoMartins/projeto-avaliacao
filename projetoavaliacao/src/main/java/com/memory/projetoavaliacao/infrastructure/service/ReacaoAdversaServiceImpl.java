package com.memory.projetoavaliacao.infrastructure.service;

import com.memory.projetoavaliacao.domain.entity.ReacaoAdversa;
import com.memory.projetoavaliacao.domain.exception.ReacaoAdversaEmUsoEception;
import com.memory.projetoavaliacao.domain.repository.ReacaoAdversaRepository;
import com.memory.projetoavaliacao.domain.service.ReacaoAdversaService;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
@RequiredArgsConstructor
public class ReacaoAdversaServiceImpl implements ReacaoAdversaService {


    private final ReacaoAdversaRepository reacaoAdversaRepository;

    @Override
    public ReacaoAdversa salvar(ReacaoAdversa reacaoAdversa) {
        return  reacaoAdversaRepository.save(reacaoAdversa);
    }

    @Override
    public ReacaoAdversa buscarPorId(Long id) {
        return reacaoAdversaRepository.findById(id).orElseThrow(()-> new
                EntityNotFoundException(String.format(" Reação adversa com id '%s' não encontrado.",id)));
    }

    @Override
    public void eliminar(Long id) {
        try {
            reacaoAdversaRepository.delete(buscarPorId(id));
        }catch (DataIntegrityViolationException e){
           throw  new ReacaoAdversaEmUsoEception(
                   String.format("ReacaoAdversaCom id '%s' não pode ser eliminada",id));
        }
    }
}
