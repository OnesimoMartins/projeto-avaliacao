package com.memory.projetoavaliacao.infrastructure.service;

import com.memory.projetoavaliacao.domain.entity.Fabricante;
import com.memory.projetoavaliacao.domain.entity.Medicamento;
import com.memory.projetoavaliacao.domain.exception.MedicamentoJaExistenteException;
import com.memory.projetoavaliacao.domain.filter.MedicamentoFiltro;
import com.memory.projetoavaliacao.domain.repository.MedicamentoRepository;
import com.memory.projetoavaliacao.domain.service.FabricanteService;
import com.memory.projetoavaliacao.domain.service.MedicamentoService;
import com.memory.projetoavaliacao.domain.service.ReacaoAdversaService;
import com.memory.projetoavaliacao.infrastructure.data.specification.MedicamentoSpecs;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;


@Service
@RequiredArgsConstructor
public class MedicamentoServiceImpl implements MedicamentoService {

    private final ReacaoAdversaService reacaoAdversaService;
    private final FabricanteService fabricanteService;
    private final MedicamentoRepository medicamentoRepository;

    @Override
    public Medicamento buscarPorNumeroRegistro(String numeroRegistro) {
        return medicamentoRepository.findById(numeroRegistro).orElseThrow(()-> new EntityNotFoundException(
                        String.format("Medicamento com número de registro '%s' não encontrado",numeroRegistro)));
    }

    @Override
    public Medicamento criar(Medicamento medicamento) {

        if (medicamentoJaExistente(medicamento.getNumeroRegistro())) {
            throw new MedicamentoJaExistenteException(
                    String.format("Medicamento com o número de registro da Anvisa"
                            + " '%s' já existente.", medicamento.getNumeroRegistro()));
        }
        return salvar(medicamento);
    }

    @Override
    public Medicamento atualizar(Medicamento medicamento) {
        return salvar(medicamento);
    }


    @Override
    public void eliminar(String numeroRegistro) {
        medicamentoRepository.delete(buscarPorNumeroRegistro(numeroRegistro));
    }

    @Override
    public Page<Medicamento> filtrarMedicamentos(MedicamentoFiltro medicamentoFiltro, Pageable pageable) {
        return medicamentoRepository.findAll(
                MedicamentoSpecs.comNomeOuNumeroRegistroSemelhante(medicamentoFiltro),pageable);
    }
    private boolean medicamentoJaExistente(String  numeroRegistro){
        return medicamentoRepository.findById(numeroRegistro).isPresent();
    }

    private Medicamento salvar(Medicamento medicamento){

        Fabricante fabricante=fabricanteService.buscarPorId(medicamento.getFabricante().getId());
        medicamento.getReacoesAdversas().forEach(r->reacaoAdversaService.buscarPorId(r.getId()));
       medicamento.setFabricante(fabricante);

        return medicamentoRepository.save(medicamento);
    }

}
