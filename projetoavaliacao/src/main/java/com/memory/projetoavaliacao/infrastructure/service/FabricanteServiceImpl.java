package com.memory.projetoavaliacao.infrastructure.service;

import com.memory.projetoavaliacao.domain.entity.Fabricante;
import com.memory.projetoavaliacao.domain.repository.FabricanteRepository;
import com.memory.projetoavaliacao.domain.service.FabricanteService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
@RequiredArgsConstructor
public class FabricanteServiceImpl implements FabricanteService {

    private final FabricanteRepository fabricanteRepository;

    @Override
    public Fabricante buscarPorId(Long id) {
        return fabricanteRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException(String.format("Fabricante com id '%s' não encontrado.", id)));
    }
}
