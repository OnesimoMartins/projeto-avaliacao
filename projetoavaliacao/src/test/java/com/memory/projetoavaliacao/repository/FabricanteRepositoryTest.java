package com.memory.projetoavaliacao.repository;


import com.memory.projetoavaliacao.domain.entity.Fabricante;
import com.memory.projetoavaliacao.domain.repository.FabricanteRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class FabricanteRepositoryTest {

    @Autowired
    private FabricanteRepository fabricanteRepository;

    @Test
    public void quandoBuscarFabricantesRetornaTodos(){
        List<Fabricante> cartoes=fabricanteRepository.findAll();
        assertTrue(cartoes.size()>=3);
    }
}
