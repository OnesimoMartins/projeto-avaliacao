package com.memory.projetoavaliacao.service;

import com.memory.projetoavaliacao.domain.entity.Fabricante;
import com.memory.projetoavaliacao.domain.entity.Medicamento;
import com.memory.projetoavaliacao.domain.entity.ReacaoAdversa;
import com.memory.projetoavaliacao.domain.exception.MedicamentoJaExistenteException;
import com.memory.projetoavaliacao.domain.repository.MedicamentoRepository;
import com.memory.projetoavaliacao.domain.service.FabricanteService;
import com.memory.projetoavaliacao.domain.service.ReacaoAdversaService;
import com.memory.projetoavaliacao.infrastructure.service.MedicamentoServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MedicamentoServiceImplTest {

    @InjectMocks
    private MedicamentoServiceImpl medicamentoService;

    @Mock
    private MedicamentoRepository medicamentoRepository;

    @Mock
    private FabricanteService fabricanteService;

    @Mock
    private ReacaoAdversaService reacaoAdversaService;

    @Test
    public void deveRetornarMedicamentoCriadoAoCriarMedicamentoNaoExistente(){

        when(medicamentoRepository.findById(getMedicamento().getNumeroRegistro()))
                .thenReturn(Optional.empty());

        when(fabricanteService.buscarPorId(1L)).thenReturn(getFabricante());

       when(reacaoAdversaService.buscarPorId(1L))
                .thenReturn(reacaoAdversaSet().stream().filter(it->it.getId()==1L).findAny().get());

        when(reacaoAdversaService.buscarPorId(2L))
                .thenReturn(reacaoAdversaSet().stream().filter(it->it.getId()==2L).findAny().get());

        when(medicamentoRepository.save(any())).thenReturn(getMedicamento());

        Medicamento medicamento=medicamentoService.criar(getMedicamento());

        assertNotNull(medicamento);
        assertNotNull(medicamento.getNumeroRegistro());
    }

    @Test
    public void deveLancarMedicamentoJaExistenteExceptioAoCriarMedicamentoJaExistente(){

        when(medicamentoRepository.findById(getMedicamento().getNumeroRegistro()))
                .thenReturn(Optional.of(getMedicamento()));

        assertThrows(MedicamentoJaExistenteException.class,
                ()->medicamentoService.criar(getMedicamento()));
    }

    @Test
    public void deveLancarEntityNotFoundExceptionAoUsarFabricanteNaoExistente(){

        when(medicamentoRepository.findById(getMedicamento().getNumeroRegistro()))
                .thenReturn(Optional.empty());

        when(fabricanteService.buscarPorId(1L)).thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class,
                ()->medicamentoService.criar(getMedicamento()));
    }
    
    public Medicamento getMedicamento(){
        return Medicamento.builder()
                .preco(BigDecimal.TEN)
                .nome("ziprofina")
                .numeroRegistro("0.0000.0000.000-0")

                .reacoesAdversas(reacaoAdversaSet())
                .fabricante(getFabricante())
                .build();
    }

    public Fabricante getFabricante(){
        return new Fabricante(1L,"fabricante");
    }

    public Set<ReacaoAdversa> reacaoAdversaSet(){
         Set<ReacaoAdversa> reacesoAdversas =new HashSet<>();
        reacesoAdversas.add( new ReacaoAdversa(1L,"reacao adversa 1"));
        reacesoAdversas.add(new ReacaoAdversa(2L,"reacao adversa 2"));
          return reacesoAdversas;
    }


}
