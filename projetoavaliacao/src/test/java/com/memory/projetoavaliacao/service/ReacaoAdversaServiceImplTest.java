package com.memory.projetoavaliacao.service;

import com.memory.projetoavaliacao.domain.entity.ReacaoAdversa;
import com.memory.projetoavaliacao.domain.repository.ReacaoAdversaRepository;
import com.memory.projetoavaliacao.infrastructure.service.ReacaoAdversaServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ReacaoAdversaServiceImplTest {

    @InjectMocks
    private ReacaoAdversaServiceImpl reacaoAdversaService;

    @Mock
    private ReacaoAdversaRepository reacaoAdversaRepository;

    @Test
    public void deveRetornarReacaoAdvercaQuandoBuscarPorId() {

        when(reacaoAdversaRepository.findById(1L)).thenReturn(Optional.of(reacaoAdversaSalva()));
        ReacaoAdversa reacaoAdversa = reacaoAdversaService.buscarPorId(1L);

        assertAll(
                () -> assertNotNull(reacaoAdversa),
                () -> assertEquals(1L, reacaoAdversa.getId())
        );
    }

    @Test
    public void deveLancarEntityNotFoundExceptionQuandoBuscarPorId() {
        when(reacaoAdversaRepository.findById(2L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class,
                () -> reacaoAdversaService.buscarPorId(2L));
    }

    @Test
    public void deveRetornarReacaoAdvercaQuandoInserirNovaReacaoAdversa(){

        ReacaoAdversa reacaoAdversa=new ReacaoAdversa();
        reacaoAdversa.setDescricao("nova reacao adversa teste");

        when(reacaoAdversaRepository.save(reacaoAdversa)).thenReturn(reacaoAdversaSalva());
        reacaoAdversa=reacaoAdversaService.salvar(reacaoAdversa);

       assertEquals(reacaoAdversa.getId(),1L);
    }



    public static ReacaoAdversa reacaoAdversaSalva() {
        ReacaoAdversa reacaoAdversa = new ReacaoAdversa();
        reacaoAdversa.setDescricao("reação adversa teste");
        reacaoAdversa.setId(1L);
        return reacaoAdversa;
    }


}
