package com.memory.projetoavaliacao.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.memory.projetoavaliacao.api.controller.MedicamentoController;
import com.memory.projetoavaliacao.api.dto.input.MedicamentoInput;
import com.memory.projetoavaliacao.api.dto.mapper.MedicamentoMapper;
import com.memory.projetoavaliacao.domain.entity.Fabricante;
import com.memory.projetoavaliacao.domain.entity.Medicamento;
import com.memory.projetoavaliacao.domain.entity.ReacaoAdversa;
import com.memory.projetoavaliacao.domain.service.MedicamentoService;
import org.assertj.core.util.Sets;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(MedicamentoController.class)
public class MedicamentoControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private MedicamentoMapper medicamentoMapper;

    @MockBean
    private MedicamentoService medicamentoService;

    @Test
    public void deveRetornar201AoIncluirMedicamento() throws Exception {
        MedicamentoInput medicamentoInput= getMedicamentoInput();

        when(medicamentoMapper.toMedicamentoDomainObject(any())).thenReturn(getMedicamento());
        when(medicamentoService.criar(any())).thenReturn(getMedicamento());

        mvc.perform(post("/medicamentos")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(medicamentoInput))
        ).andExpect(status().isCreated());

    }

    @Test
    public void deveRetornar400AoIncluirMedicamentoComBodyInvalido() throws Exception {
        MedicamentoInput medicamentoInput= getMedicamentoInput();
        medicamentoInput.setNumeroRegistro("(1a)9800-0088");

        when(medicamentoMapper.toMedicamentoDomainObject(any())).thenReturn(getMedicamento());
        when(medicamentoService.criar(any())).thenReturn(getMedicamento());

        mvc.perform(post("/medicamentos")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(medicamentoInput)))
                .andExpect(status().isBadRequest());
    }

    private MedicamentoInput getMedicamentoInput(){
        return MedicamentoInput.builder()
                .nome("ziprofina")
                .numeroRegistro("0.0000.2000.001-0")
                .telefoneSac("(11)9800-0088")
                .preco(BigDecimal.TEN)
                .quantidadeComprimidos(14)
                .dataValidade(new Date(1, Calendar.JANUARY,2011))
                .fabricanteId(1L)
                .reacoesAdversasIds(Sets.set(1L,2L))
                .build();
    }

    @Test
    public void deveRetornar200AoAtualizarMedicamento() throws Exception {
        MedicamentoInput medicamentoInput= getMedicamentoInput();
        Medicamento medicamentoActual=getMedicamento();
        Medicamento medicamentoNovo=getMedicamento();
        medicamentoNovo.setPreco(BigDecimal.ZERO);

        when(medicamentoMapper.copiarParaMedicamento(medicamentoInput,medicamentoActual)).thenReturn(medicamentoNovo);
        when(medicamentoService.atualizar(any())).thenReturn(medicamentoNovo);

        mvc.perform(put("/medicamentos/0.0000.2000.001-0")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(medicamentoInput))
        ).andExpect(status().isOk());

    }


    public Medicamento getMedicamento(){
        return Medicamento.builder()
                .nome("ziprofina")
                .numeroRegistro("0.0000.2000.001-0")
                .telefoneSac("(11)9800-0088")
                .preco(BigDecimal.TEN)
                .quantidadeComprimidos(14)
                .dataValidade(new Date(1, Calendar.JANUARY,2011))
                .fabricante(new Fabricante(1L,null))
                .reacoesAdversas(Sets.set(
                        new ReacaoAdversa(1L,"aa"),
                        new ReacaoAdversa(2L,"bb")))
                .build();
    }


}
