package com.memory.projetoavaliacao.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.memory.projetoavaliacao.api.controller.ReacaoAdversaController;
import com.memory.projetoavaliacao.api.dto.input.ReacaoAdversaInput;
import com.memory.projetoavaliacao.api.dto.mapper.ReacaoAdversaMapper;
import com.memory.projetoavaliacao.api.dto.output.ReacaoAdversaOutput;
import com.memory.projetoavaliacao.domain.entity.ReacaoAdversa;
import com.memory.projetoavaliacao.domain.service.ReacaoAdversaService;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ReacaoAdversaController.class)
public class ReacaoAdversaControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private ReacaoAdversaService reacaoAdversaService;

    @MockBean
    private ReacaoAdversaMapper reacaoAdversaMapper;

    @Test
    public void deveRetornar201AoIncluirReacaoAdversa() throws Exception {
        ReacaoAdversaInput novaReacaoAdversa= new ReacaoAdversaInput("nova reacao adversa");

        when(reacaoAdversaMapper.toReacaoAdversa(novaReacaoAdversa)).thenReturn(getReacaoAdversaSemId());
        when(reacaoAdversaService.salvar(getReacaoAdversaSemId())).thenReturn(getReacaoAdversaComId());
        when(reacaoAdversaMapper.toReacaoAdversaOutput(getReacaoAdversaComId())).thenReturn(getReacaoAdversaOutput());

        mvc.perform(post("/reacoes-adversas")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsBytes(novaReacaoAdversa))
                ).andExpect(status().isCreated());

    }

    @Test
    public void deveRetornar200AoActualizarReacaoAdversa() throws Exception {
        ReacaoAdversaInput novaReacaoAdversa= new ReacaoAdversaInput("actualizar reacao adversa");

        when(reacaoAdversaMapper.toReacaoAdversa(novaReacaoAdversa)).thenReturn(getReacaoAdversaSemId());
        when(reacaoAdversaService.salvar(getReacaoAdversaSemId())).thenReturn(getReacaoAdversaComId());
        when(reacaoAdversaMapper.toReacaoAdversaOutput(getReacaoAdversaComId())).thenReturn(getReacaoAdversaOutput());

        mvc.perform(put("/reacoes-adversas/1")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(novaReacaoAdversa))
        ).andExpect(status().isOk());
    }

    @Test
    public void deveRetornar204AoEliminarReacaoAdversa() throws Exception {

        Mockito.doNothing().when(reacaoAdversaService).eliminar(1L);

        mvc.perform(delete("/reacoes-adversas/1")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isNoContent());
    }


    private ReacaoAdversa getReacaoAdversaComId() {
        ReacaoAdversa reacaoAdversa=new ReacaoAdversa();
        reacaoAdversa.setDescricao("nova reacao adversa");
        reacaoAdversa.setId(1L);

        return reacaoAdversa;
    }

    private ReacaoAdversa getReacaoAdversaSemId() {
      ReacaoAdversa reacaoAdversa=new ReacaoAdversa();
      reacaoAdversa.setDescricao("nova reacao adversa");

      return reacaoAdversa;
    }

    private ReacaoAdversaOutput getReacaoAdversaOutput() {
        return ReacaoAdversaOutput.builder()
                .id(1L).descricao("nova reacao adversa")
                .build();
    }

}
